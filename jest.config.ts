import { pathsToModuleNameMapper } from 'ts-jest';

export default {
	preset: 'ts-jest',
	moduleDirectories: ['node_modules', '<rootDir>'],
	moduleNameMapper: pathsToModuleNameMapper({
		'@/*': ['src/*'],
	}),
	collectCoverageFrom: ['src/**/*.{ts,vue}', '!**/node_modules/**', '!**/tests/**'],
	coverageDirectory: '<rootDir>/coverage',
	coverageReporters: ['cobertura', 'text'],
};
